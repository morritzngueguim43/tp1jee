package com.merritz.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.merritz.beans.Client;
import com.merritz.beans.Commande;

public class CreationCommande extends HttpServlet {
	public void doPost( HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		String nom = request.getParameter( "nomClient" );
		String prenom = request.getParameter( "prenomClient" );
		String adresse = request.getParameter( "adresseClient" );
		String telephone = request.getParameter( "telephoneClient");
		String email = request.getParameter( "emailClient" );
		Date date = new Date();
		double montant;
		try {
			/* R�cup�ration du montant */
			montant = Double.parseDouble( request.getParameter("montantCommande" ) );
		} catch ( NumberFormatException e ) {
			/* Initialisation � -1 si le montant n'est pas un nombre correct */
			montant = -1;
		}
		
		String modePaiement = request.getParameter("modePaiementCommande" );
		String statutPaiement = request.getParameter("statutPaiementCommande" );
		String modeLivraison = request.getParameter("modeLivraisonCommande" );
		String statutLivraison = request.getParameter("statutLivraisonCommande" );
		String message;

		if ( nom.trim().isEmpty() || adresse.trim().isEmpty() ||telephone.trim().isEmpty() || montant == -1|| modePaiement.isEmpty() ||modeLivraison.isEmpty() ) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerCommande.jsp\">Cliquez ici</a> pour acc�der au formulaire de cr�ation d'une commande.";
		} else {
			message = "Commande cr��e avec succ�s !";
		}
		
		Client client = new Client();
		client.setNom( nom );
		client.setPrenom( prenom );
		client.setAdresse( adresse );
		client.setTelephone( telephone );
		client.setEmail( email );
		Commande commande = new Commande();
		commande.setClient( client );
		commande.setDate( date.toString() );
		commande.setMontant( montant );
		commande.setModePaiement( modePaiement );
		commande.setStatutPaiement( statutPaiement );
		commande.setModeLivraison( modeLivraison );	
		commande.setStatutLivraison( statutLivraison );
		
		/* Ajout du bean et du message � l'objet requ�te */
		request.setAttribute( "commande", commande );
		request.setAttribute( "message", message );

		/* Transmission � la page JSP en charge de l'affichage des donn�es */
		this.getServletContext().getRequestDispatcher("/afficherCommande.jsp" ).forward( request, response );
	}
}