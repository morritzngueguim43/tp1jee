package com.merritz.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.merritz.beans.Client;

public class CreationClient extends HttpServlet {
	public void doGet( HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		String nom = request.getParameter( "nomClient" );
		String prenom = request.getParameter( "prenomClient" );
		String adresse = request.getParameter( "adresseClient" );
		String telephone = request.getParameter( "telephoneClient");
		String email = request.getParameter( "emailClient" );
		String message;
		if ( nom.trim().isEmpty() || adresse.trim().isEmpty() ||telephone.trim().isEmpty() ) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerClient.jsp\">Cliquez ici</a> pour acc�der au formulaire de cr�ation d'un client.";
		} else {
			message = "Client cr�� avec succ�s !";
		}
		
		Client client = new Client();
		client.setNom( nom );
		client.setPrenom( prenom );
		client.setAdresse( adresse );
		client.setTelephone( telephone );
		client.setEmail( email );
		
		/* Ajout du bean et du message � l'objet requ�te */
		request.setAttribute( "client", client );
		request.setAttribute( "message", message );

		/* Transmission � la page JSP en charge de l'affichage des donn�es */
		this.getServletContext().getRequestDispatcher("/afficherClient.jsp" ).forward( request, response );
	}
}